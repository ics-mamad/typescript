interface mainUser {
    readonly dbId:number,
    email:string,
    userId:number,
    googleId?:number, //google id is optional thing ?
    startTrial():string,
    getCoupen(courseName:string): number
}

interface mainUser{
    githubid:string
}

interface Admin extends mainUser{
    role: "admin" | "tl" | "manager" | "senior"
}

const newUser1 :mainUser = {
    dbId:1,email:"a@a.com",userId:3311,googleId:11,githubid:"git1",
    startTrial:()=>{
        return "TS"
    },
    getCoupen:(cpn_name:"Alyani10")=>{
        return 10
    } 
}

newUser1.email = "alyani@gmail.com"
// newUser1.dbId=2

export {}