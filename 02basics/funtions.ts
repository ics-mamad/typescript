function addTwo(num: number){
    // return 2 + num
    return "Alyani" //this should not happen
}

function ToUpper(str:string){
    // return str.toUpperCase()
    return 5 //this should not happen
}

function IsSignedUp(name:string,email:string,paid:boolean = false){
    return `${name} with email ${email} is${paid ? "" : " not"} paid`
}


//more than one return type(different-different)

// function getVal(num:number):boolean{
//     if(num <5){
//         return true
//     }else{
//         return "Hello"
//     }
// }
// getVal(10)
// getVal(1)


addTwo(5)
ToUpper("Alyani")
IsSignedUp("Alyani","a@m.com")

//playing with array

const arr1 = ['a','b','c','d']
const arr2 = [1,2,3,4]

arr1.map((arr):string=>{
    // return 1
    return ""
})

arr2.map((arr):number=>{
    // return ""
    return 1
})


function consoleError(errmsg:string):void{
    console.log(errmsg)
}

function handleError(errmsg:string):never{
    throw new Error(errmsg)
}


export {}






