function addTwo(num) {
    // return 2 + num
    return "Alyani"; //this should not happen
}
function ToUpper(str) {
    // return str.toUpperCase()
    return 5; //this should not happen
}
function IsSignedUp(name, email, paid) {
    if (paid === void 0) { paid = false; }
    return "".concat(name, " with email ").concat(email, " is").concat(paid ? "" : " not", " paid");
}
//more than one return type(different-different)
// function getVal(num:number):boolean{
//     if(num <5){
//         return true
//     }else{
//         return "Hello"
//     }
// }
// getVal(10)
// getVal(1)
addTwo(5);
ToUpper("Alyani");
IsSignedUp("Alyani", "a@m.com");
//playing with array
var arr1 = ['a', 'b', 'c', 'd'];
var arr2 = [1, 2, 3, 4];
arr1.map(function (arr) {
    // return 1
    return "";
});
arr2.map(function (arr) {
    // return ""
    return 1;
});
function consoleError(errmsg) {
    console.log(errmsg);
}
function handleError(errmsg) {
    throw new Error(errmsg);
}
