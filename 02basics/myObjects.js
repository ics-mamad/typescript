"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = {
    name: "Alyani",
    email: "a@a.com",
    isActive: true
};
function createUser(_a) {
    var string = _a.name, boolean = _a.isPaid;
}
// createUser({name:"Alyani",isPaid:false,email:"a@a.com"}) //this will give error
var newUser = { name: "Alyani", isPaid: true, email: "a@a.com" }; //this will work fine
createUser(newUser);
function createCourse() {
    return { courseName: "ReactJS", price: 499 };
}
function createUser1(user) {
    return { name: "", email: "", isPaid: true };
}
createUser1({ name: "Alyani", email: "a@a.com", isPaid: true });
