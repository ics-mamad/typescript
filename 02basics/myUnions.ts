let score: number | string
score = 33
score = "33"

type User1 = {
    name: string,
    id:number
}

type Admin = {
    username:string,
    id:number
}

let alyani:User1 | Admin = {name:"Alyani",id:72}
alyani = {username:"AMB",id:72}


function getId(id:number | string){
    // id.toLocaleLowerCase() //this wont work directly
    // id.toFixed() //this wont work directly

    if(typeof id === "string"){
        id.toLocaleLowerCase()
    }else{
        id.toFixed()
    }
}

getId("55")
getId(55)



//Arrays

const arr1:number[] = [1,2,3,4,5]
const arr2:string[] = ["1","2","3","4","5"]
const arr3:(string | number | boolean)[] = [1,2,3,"4","5",true,false]


let pi:3.14 = 3.14

// pi=3.145 //this wont work