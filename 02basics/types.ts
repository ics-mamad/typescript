//usecase-1

type User = {
    readonly _id:string, //read only 
    name:string,
    email:string
    isActive:boolean
}

let myUser:User = {
    _id:"1",
    name:"Alyani",
    email:"A@a.com",
    isActive:false
}

myUser.email = "alyani@gmail.com"
// myUser._id = "2" //this will give error



//usecase-2
type cardNumber = {
    cardnumber: number
}

type cardDate = {
    carddate:string
}

type cardCvv = {
    cardcvv:string
}

type cardDetails = cardNumber & cardDate & cardCvv