const User ={
    name:"Alyani",
    email:"a@a.com",
    isActive:true
}


function createUser({name:string,isPaid:boolean}){}

// createUser({name:"Alyani",isPaid:false,email:"a@a.com"}) //this will give error

let newUser = {name:"Alyani",isPaid:true,email:"a@a.com"} //this will work fine

createUser(newUser)


function createCourse():{courseName:string,price:number}{
    return {courseName:"ReactJS",price:499}
}


//type alias

type User = {
    name :string,
    email :string,
    isPaid :boolean
}

function createUser1(user:User):User{
    return {name:"",email:"",isPaid:true}
}

createUser1({name:"Alyani",email:"a@a.com",isPaid:true})

export {}


