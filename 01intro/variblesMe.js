"use strict";
// string
Object.defineProperty(exports, "__esModule", { value: true });
var greet = "Hello World";
greet = "Hello Alyani";
//  greet = 72  // this will give an error
//number
var no1 = 10;
var no2 = 20.20;
var no3 = 20.21212;
// no3 = "Alyani" //this will give an error
no1.toFixed();
no2.toFixed();
//boolean
var isLoggedIn = false;
greet.toLocaleLowerCase();
console.log(greet);
