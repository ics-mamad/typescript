// string

let greet :string = "Hello World";

greet = "Hello Alyani"

//  greet = 72  // this will give an error


//number

let no1 :number = 10
let no2 :number = 20.20
let no3 = 20.21212
// no3 = "Alyani" //this will give an error

no1.toFixed()
no2.toFixed()


//boolean

let isLoggedIn :boolean = false

//any

let hero

function getHero() {
    return "Superman"
}

hero = getHero()


greet.toLocaleLowerCase()

console.log(greet)

export {}